---
layout: post
title:  "Arithmetic Series"
date:   2016-03-24 15:32:14 -0300
categories: series
---

This is an arithmetic series.
$$\sum_{x = a}^{b} f(x)$$
